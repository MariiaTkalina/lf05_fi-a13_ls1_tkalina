import java.util.Scanner;

public class Fallunterscheidungen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);

		double ergebnis = 0.0;
		double x;
		double y;
		char operators;

		System.out.println("Taschenrechner\n");

		System.out.println("Ich kann nur addieren, subtrahieren, multiplizieren oder dividieren.\n");

		System.out.println("Geben Sie bitte 2 Zahlen ein, um zu berechnen.\n");
		System.out.println("Zahl 1:");
		x = tastatur.nextDouble();

		System.out.println("Zahl 2:");
		y = tastatur.nextDouble();

		do {
			System.out.println("W�hlen Sie bitte Operation '+' oder '-' oder '*' oder '/'");
			operators = tastatur.next().charAt(0);
			if (operators != '+' && operators != '+' && operators != '-' && operators != '/') {
				System.out.println("Falsche Operation");
			}
		} while (operators != '+' && operators != '+' && operators != '-' && operators != '/');

		if (operators == '+') {
			ergebnis = x + y;
		} else if (operators == '-') {
			ergebnis = x - y;
		} else if (operators == '*') {
			ergebnis = x * y;
		} else if (operators == '/') {
			ergebnis = x / y;
		}

		System.out.println("Das Ergebnis ist: " + ergebnis);

	}

}
