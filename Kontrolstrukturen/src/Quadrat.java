import java.util.Scanner;

public class Quadrat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);

		int sideLength;
		System.out.println("Quadrat");
		System.out.println("Geben Sie bitte die Seitenlšnge eines Quadrats: ");
		sideLength = tastatur.nextInt();

		System.out.println("\n");

		for (int i = 0; i < sideLength; i++) {
			for (int j = 0; j < sideLength; j++) {
				if (j == 0 || j == sideLength - 1 || i == 0 || i == sideLength - 1) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}

			System.out.println("");
		}

	}
}
