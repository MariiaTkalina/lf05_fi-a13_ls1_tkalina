package konsolenausgaben;

public class Aufgabe2 {

	public static void main(String[] args) {
		
		String s = "*";
		System.out.printf( "%7s\n", s ); 
		
		String s1 = "***";
		System.out.printf( "%8s\n", s1 ); 
		
		String s2 = "*****";
		System.out.printf( "%9s\n", s2 ); 
		
		String s3 = "*******";
		System.out.printf( "%10s\n", s3 ); 
		
		String s4 = "*********";
		System.out.printf( "%11s\n", s4 ); 
		
		String s5 = "***********";
		System.out.printf( "%12s\n", s5 ); 
		
		String s6 = "*************";
		System.out.printf( "%13s\n", s6 ); 
		
		String s7 = "***";
		System.out.printf( "%8s\n", s7 ); 
		
		String s8 = "***";
		System.out.printf( "%8s\n", s8 ); 
	}
}
