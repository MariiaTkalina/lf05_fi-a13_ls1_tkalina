package konsolenausgaben;

public class Aufgabe4 {

	public static void main(String[] args) {
		
		String s = "**";
		System.out.printf( "%5s\n", s ); 
		
		String s1 = "*";
		System.out.printf( "%1s", s1 ); 
		
		String s2 = "*";
		System.out.printf( "%7s\n", s2 ); 
		
		String s3 = "*";
		System.out.printf( "%1s", s3 ); 
		
		String s4 = "*";
		System.out.printf( "%7s\n", s4 ); 
		
		String s5 = "**";
		System.out.printf( "%5s\n", s5 ); 
		
		
	}

}
