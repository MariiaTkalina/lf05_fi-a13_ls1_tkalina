import java.util.Scanner;

class Fahrkartenautomat {

	// Benutzen Sie f�r die Verwaltung der Fahrkartenbezeichnung und der
	// Fahrkartenpreise jeweils ein Array.
	// Welche Vorteile hat man durch diesen Schritt?
	//Ich kann neue Arten von Tickets hinzuf�gen oder vorhandene entfernen, ohne das Programm zu �ndern, ich �ndere nur die Arrays selbst

	// 1
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		int wahl;
		double zuZahlenderBetrag = 0.0;

		String[] fahrkartenbezeichnung = new String[10];
		fahrkartenbezeichnung[0] = " Einzelfahrschein Berlin AB        ";
		fahrkartenbezeichnung[1] = " Einzelfahrschein Berlin BC        ";
		fahrkartenbezeichnung[2] = " Einzelfahrschein Berlin ABC       ";
		fahrkartenbezeichnung[3] = " Kurzstrecke                       ";
		fahrkartenbezeichnung[4] = " Tageskarte Berlin AB              ";
		fahrkartenbezeichnung[5] = " Tageskarte Berlin BC              ";
		fahrkartenbezeichnung[6] = " Tageskarte Berlin ABC             ";
		fahrkartenbezeichnung[7] = " Kleingruppen-Tageskarte Berlin AB  ";
		fahrkartenbezeichnung[8] = " Kleingruppen-Tageskarte Berlin BC  ";
		fahrkartenbezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC ";

		double[] fahrkartenpreise = new double[10];
		fahrkartenpreise[0] = 2.90;
		fahrkartenpreise[1] = 3.30;
		fahrkartenpreise[2] = 3.60;
		fahrkartenpreise[3] = 1.90;
		fahrkartenpreise[4] = 8.60;
		fahrkartenpreise[5] = 9.00;
		fahrkartenpreise[6] = 9.60;
		fahrkartenpreise[7] = 23.50;
		fahrkartenpreise[8] = 24.30;
		fahrkartenpreise[9] = 24.90;

		int count = 1;

		System.out.println("Auswahlnummer	     Bezeichnung	                           Preis in Euro\n");
		for (int f = 0; f < fahrkartenbezeichnung.length; f++) {
			System.out.printf(count++ + "                   " + fahrkartenbezeichnung[f] + "%15s\n",
					fahrkartenpreise[f]);
		}

		System.out.println("\n");

		do {
			System.out.println("Ihre Wahl: ");
			wahl = tastatur.nextInt();

			if (wahl < 1 || wahl > 10) {
				System.out.print(">>falsche Eingabe<<\n>>W�hlen Sie ihre Wunschfahrkarte 1 oder 2 oder 3<<\n");
			}
		} while (wahl != 1 && wahl != 2 && wahl != 3 && wahl != 4 && wahl != 5 && wahl != 6 && wahl != 7 && wahl != 8
				&& wahl != 9 && wahl != 10);

		if (wahl == 1) {
			zuZahlenderBetrag = 2.90;
		} else if (wahl == 2) {
			zuZahlenderBetrag = 3.30;
		} else if (wahl == 3) {
			zuZahlenderBetrag = 3.60;
		} else if (wahl == 4) {
			zuZahlenderBetrag = 1.90;
		} else if (wahl == 5) {
			zuZahlenderBetrag = 8.60;
		} else if (wahl == 6) {
			zuZahlenderBetrag = 9.00;
		} else if (wahl == 7) {
			zuZahlenderBetrag = 9.60;
		} else if (wahl == 8) {
			zuZahlenderBetrag = 23.50;
		} else if (wahl == 9) {
			zuZahlenderBetrag = 24.30;
		} else if (wahl == 10) {
			zuZahlenderBetrag = 24.90;
		} else {
			System.out.print(">>falsche Eingabe<<\n>>W�hlen Sie ihre Wunschfahrkarte Nummer 1 bis 10<<");
		}

		int tiketsAnzahl;

		do {
			System.out.println("Ticketanzahl: ");
			tiketsAnzahl = tastatur.nextInt();

			if (tiketsAnzahl > 10) {
				System.out.print(">>Nehmen Sie bitte nicht mehr als 10 Tickets<<\n");
			} else if (tiketsAnzahl <= 0) {
				System.out.print(">>Falsche Eingabe<<\n");
			}
		} while (tiketsAnzahl > 10 || tiketsAnzahl <= 0);

		return zuZahlenderBetrag * tiketsAnzahl;
	}

	// 2
	public static double fahrkartenBezahlen(double insgesamterBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < insgesamterBetrag) {
			double eingeworfeneM�nze;

			do {
				System.out.printf("Noch zu zahlen: %.2f Euro\n", insgesamterBetrag - eingezahlterGesamtbetrag);
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
				
				eingeworfeneM�nze = tastatur.nextDouble();

				
				if (eingeworfeneM�nze != 0.10 && eingeworfeneM�nze != 0.20 && eingeworfeneM�nze != 0.50
						&& eingeworfeneM�nze != 1 && eingeworfeneM�nze != 2) {
					System.out.print("Falsche eingegebene M�nze. Versuchen Sie bitte nochmal. \n");
					System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
				}
			} while (eingeworfeneM�nze != 0.10 && eingeworfeneM�nze != 0.20 && eingeworfeneM�nze != 0.50
					&& eingeworfeneM�nze != 1 && eingeworfeneM�nze != 2);
			
			eingezahlterGesamtbetrag += eingeworfeneM�nze;

		}
		return eingezahlterGesamtbetrag;
	}

	// 3
	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 18; i++) {
			System.out.print("=");

			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("\n\n");
	}

	// 4
	public static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double insgesamterBetrag) {

		double r�ckgabebetrag = eingezahlterGesamtbetrag - insgesamterBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro ", r�ckgabebetrag);

			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("     ***     ");
				System.out.println("  *       *     ");
				System.out.println(" *    2    *     ");
				System.out.println(" *   EURO  *     ");
				System.out.println("  *       *     ");
				System.out.println("     ***     ");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("     ***     ");
				System.out.println("  *       *     ");
				System.out.println(" *    1    *     ");
				System.out.println(" *   EURO  *     ");
				System.out.println("  *       *     ");
				System.out.println("     ***     ");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("     ***     ");
				System.out.println("  *       *     ");
				System.out.println(" *    50   *     ");
				System.out.println(" *   CENT  *     ");
				System.out.println("  *       *     ");
				System.out.println("     ***     ");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("     ***     ");
				System.out.println("  *       *     ");
				System.out.println(" *    20   *     ");
				System.out.println(" *   CENT  *     ");
				System.out.println("  *       *     ");
				System.out.println("     ***     ");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("     ***     ");
				System.out.println("  *       *     ");
				System.out.println(" *    10   *     ");
				System.out.println(" *   CENT  *     ");
				System.out.println("  *       *     ");
				System.out.println("     ***     ");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("     ***     ");
				System.out.println("  *       *     ");
				System.out.println(" *    5    *     ");
				System.out.println(" *   CENT  *     ");
				System.out.println("  *       *     ");
				System.out.println("     ***     ");
				r�ckgabebetrag -= 0.05;
			}
		}

		return r�ckgabebetrag;
	}

	// main
	public static void main(String[] args) {
		while (true) {

			System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin ABC aus:\n");
			// + " Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
			// + " Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
			// + " Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");

			double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			double bezahlt = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(bezahlt, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n"
					+ "=====================================================================================\n\n");

		}
	}

}